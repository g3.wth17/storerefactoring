package store;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Order {

	private Customer customer;
	private Salesman salesman;
	private Date orderedOn;
	private String deliveryStreet;
	private String deliveryCity;
	private String deliveryCountry;
	private Set<OrderItem> items;

	public Order(Customer customer, Salesman salesman, String deliveryStreet, String deliveryCity, String deliveryCountry, Date orderedOn) {
		this.customer = customer;
		this.salesman = salesman;
		this.deliveryStreet = deliveryStreet;
		this.deliveryCity = deliveryCity;
		this.deliveryCountry = deliveryCountry;
		this.orderedOn = orderedOn;
		this.items = new HashSet<OrderItem>();
	}

	public Customer getCustomer() {
		return customer;
	}

	public Salesman getSalesman() {
		return salesman;
	}

	public Date getOrderedOn() {
		return orderedOn;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public String getDeliveryCountry() {
		return deliveryCountry;
	}

	public Set<OrderItem> getItems() {
		return items;
	}

	public float total() {
		float total = 0;
		float totalItems = 0;
		for (OrderItem item : items) {
			float totalItem = calculatePriceWithDiscount(item);
			totalItems += totalItem;
		}
		total = totalItems + calculeTax(totalItems);
		
		if (needsShippingCost()){
			total =  total + 15 ;
		}
		return total;
		
	}

	private boolean needsShippingCost() {
		return this.deliveryCountry != "USA";
	}

	private float calculeTax(float totalItems) {
		return totalItems * 5 / 100;
	}

	private float calculatePriceWithDiscount(OrderItem item) {
		float discount = 0; 
		float itemAmount = calculateItemAmount(item);
		if (belongsToAccesories(item)) 
			discount = calculateDiscountForAccesories(itemAmount);
		if (belongsToBikes(item))
			discount = calculateDiscountForBikes(itemAmount);
		if (belongsToClothing(item))
			discount = calculateDiscountForClothing(item);
		return itemAmount - discount;
	}

	private boolean belongsToClothing(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Cloathing;
	}

	private boolean belongsToBikes(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Bikes;
	}

	private boolean belongsToAccesories(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Accessories;
	}

	private float calculateItemAmount(OrderItem item) {
		return item.getProduct().getUnitPrice() * item.getQuantity();
	}

	private float calculateDiscountForClothing(OrderItem item) {
		float cloathingDiscount = 0;
		if (item.getQuantity() > 2) {
			cloathingDiscount = item.getProduct().getUnitPrice();
		}
		return cloathingDiscount;
	}

	private float calculateDiscountForBikes(float itemAmount) {
		//20% discount in bikes
		return itemAmount * 20 / 100;
	}

	private float calculateDiscountForAccesories(float itemAmount) {
		float booksDiscount = 0;
		if (itemAmount >= 100) {
			booksDiscount = itemAmount * 10 / 100;
		}
		return booksDiscount;
	}
}
